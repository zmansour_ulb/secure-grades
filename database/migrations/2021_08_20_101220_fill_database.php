<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Hash;

use App\Models\User;
use App\Models\Course;
use App\Models\CourseStudent;
use App\Models\CourseTeacher;

class FillDatabase extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Administrator
        $admin = User::create([
            'status' => 'ADM',
            'firstname' => 'Zeus',
            'lastname' => 'McAdmin',
            'email' => 'zeus.admin@uni.be',
            'password' => Hash::make('zg1T1J97Sx.k7BYjVcFaj'),
        ]);
        // Teachers
        $teacher0 = User::create([
            'status' => 'TEA',
            'firstname' => 'Appolo',
            'lastname' => 'McTeacher',
            'email' => 'appolo.teacher@uni.be',
            'password' => Hash::make('kwz2AKBrQ0.rz7mRwiUBd'),
        ]);
        $teacher1 = User::create([
            'status' => 'TEA',
            'firstname' => 'Artemis',
            'lastname' => 'McTeacher',
            'email' => 'artemis.teacher@uni.be',
            'password' => Hash::make('nVPtexWFqc.7e02XzfMyo'),
        ]);
        // Students
        $student0 = User::create([
            'status' => 'STU',
            'firstname' => 'Ares',
            'lastname' => 'McStudent',
            'email' => 'ares.student@uni.be',
            'password' => Hash::make('UUcU5Yeggo.IT3ESKlC3u'),
        ]);
        $student1 = User::create([
            'status' => 'STU',
            'firstname' => 'Demeter',
            'lastname' => 'McStudent',
            'email' => 'demeter.student@uni.be',
            'password' => Hash::make('O02eg447Hf.8cN3Re3Uo1'),
        ]);
        $student2 = User::create([
            'status' => 'STU',
            'firstname' => 'Dionysus',
            'lastname' => 'McStudent',
            'email' => 'dionysus.student@uni.be',
            'password' => Hash::make('QXiSdn4Q9M.0PtiIO9Gb2'),
        ]);
        $student3 = User::create([
            'status' => 'STU',
            'firstname' => 'Hades',
            'lastname' => 'McStudent',
            'email' => 'hades.student@uni.be',
            'password' => Hash::make('FMgWGo2BZL.UXUSne1mcY'),
        ]);
        $student4 = User::create([
            'status' => 'STU',
            'firstname' => 'Hephaestus',
            'lastname' => 'McStudent',
            'email' => 'hephaestus.student@uni.be',
            'password' => Hash::make('jq9zVsUdpk.eRHUOrdxRj'),
        ]);
        $student5 = User::create([
            'status' => 'STU',
            'firstname' => 'Hera',
            'lastname' => 'McStudent',
            'email' => 'hera.student@uni.be',
            'password' => Hash::make('GkDTiVF8sR.Zsdca8PQDz'),
        ]);
        $student6 = User::create([
            'status' => 'STU',
            'firstname' => 'Hermes',
            'lastname' => 'McStudent',
            'email' => 'hermes.student@uni.be',
            'password' => Hash::make('TVZgbCFWtG.By1B5UkRy2'),
        ]);
        $student7 = User::create([
            'status' => 'STU',
            'firstname' => 'Hestia',
            'lastname' => 'McStudent',
            'email' => 'hestia.student@uni.be',
            'password' => Hash::make('EOwdYU9fq7.BJcOnZd1LE'),
        ]);
        $student8 = User::create([
            'status' => 'STU',
            'firstname' => 'Poseidon',
            'lastname' => 'McStudent',
            'email' => 'poseidon.student@uni.be',
            'password' => Hash::make('1YyRH8Mv6E.CTkvhaU2FQ'),
        ]);
        $student9 = User::create([
            'status' => 'STU',
            'firstname' => 'Aphrodite',
            'lastname' => 'McStudent',
            'email' => 'aphrodite.student@uni.be',
            'password' => Hash::make('nQANUN5bc6.Ce3kYO0lTS'),
        ]);
        // Courses
        $course0 = Course::create([
            'label' => 'MATH-H100 Analysis I',
        ]);
        $course1 = Course::create([
            'label' => 'MECA-H200 Mechanics II',
        ]);
        $course2 = Course::create([
            'label' => 'ELEC-H310 Digital Electronics',
        ]);
        // Course Teachers
        CourseTeacher::create([
            'courseId' => $course0->id,
            'teacherId' => $teacher0->id,
        ]);
        CourseTeacher::create([
            'courseId' => $course2->id,
            'teacherId' => $teacher0->id,
        ]);
        CourseTeacher::create([
            'courseId' => $course1->id,
            'teacherId' => $teacher1->id,
        ]);
        CourseTeacher::create([
            'courseId' => $course2->id,
            'teacherId' => $teacher1->id,
        ]);
        // Course Students
        CourseStudent::create([
            'courseId' => $course0->id,
            'studentId' => $student0->id,
            'grade' => 20,
        ]);
        CourseStudent::create([
            'courseId' => $course0->id,
            'studentId' => $student1->id,
            'grade' => 0,
        ]);
        CourseStudent::create([
            'courseId' => $course0->id,
            'studentId' => $student3->id,
        ]);
        CourseStudent::create([
            'courseId' => $course0->id,
            'studentId' => $student5->id,
        ]);
        CourseStudent::create([
            'courseId' => $course0->id,
            'studentId' => $student7->id,
            'grade' => 12,
        ]);
        CourseStudent::create([
            'courseId' => $course0->id,
            'studentId' => $student8->id,
            'grade' => 18,
        ]);
        CourseStudent::create([
            'courseId' => $course0->id,
            'studentId' => $student9->id,
            'grade' => 5,
        ]);
        CourseStudent::create([
            'courseId' => $course1->id,
            'studentId' => $student1->id,
            'grade' => 10,
        ]);
        CourseStudent::create([
            'courseId' => $course1->id,
            'studentId' => $student2->id,
            'grade' => 12,
        ]);
        CourseStudent::create([
            'courseId' => $course1->id,
            'studentId' => $student3->id,
            'grade' => 14,
        ]);
        CourseStudent::create([
            'courseId' => $course1->id,
            'studentId' => $student8->id,
        ]);
        CourseStudent::create([
            'courseId' => $course1->id,
            'studentId' => $student9->id,
        ]);
        CourseStudent::create([
            'courseId' => $course2->id,
            'studentId' => $student2->id,
        ]);
        CourseStudent::create([
            'courseId' => $course2->id,
            'studentId' => $student4->id,
        ]);
        CourseStudent::create([
            'courseId' => $course2->id,
            'studentId' => $student8->id,
            'grade' => 8,
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('course_students');
    }
}