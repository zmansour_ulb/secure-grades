# 1. Install required softwares
sudo apt-get update -y
sudo apt-get install -y php7.4-cli mysql-server php7.4-mysql zip unzip libxml2-dev libzip-dev nodejs npm
sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password password THIS15.a.super.root.PASS' # changing root blank pwd
sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password_again password THIS15.a.super.root.PASS' # changing root blank pwd
curl -sS https://getcomposer.org/installer | php
chmod +x composer.phar
sudo mv composer.phar /usr/bin/composer
composer global require enlightn/security-checker
sudo npm cache clean -f
sudo npm install -g n
sudo n stable
sudo npm install npm@latest -g

# 2. Create DB/user
PASS="THIS15.a.super.PASS"
USER="gradesuser"
DB="gradesdb"
sudo sh -c "mysql -uroot <<MYSQL_SCRIPT
CREATE DATABASE $DB;
CREATE USER '$USER'@'localhost' IDENTIFIED BY '$PASS';
GRANT ALL PRIVILEGES ON $DB.* TO '$USER'@'localhost';
FLUSH PRIVILEGES;
MYSQL_SCRIPT"
echo "MySQL user created."
echo "DB name:    $DB"
echo "Username:   $USER"
echo "Password:   $PASS"

# 3. Deploy website
sudo apt install -y apache2 php libapache2-mod-php php-pdo php-mbstring php-tokenizer php-xml php7.4-gd
sudo a2dissite 000-default.conf
sudo sh -c 'cat > /etc/apache2/sites-available/securegrades.conf << EOF
<Directory /var/www/secure-grades/public>
    AllowOverride All
    Require all granted
</Directory>
<VirtualHost *:443>
    DocumentRoot /var/www/secure-grades/public

    SSLEngine on
    SSLCertificateFile "/var/www/secure-grades/ssl/localhost.crt"
    SSLCertificateKeyFile "/var/www/secure-grades/ssl/localhost.key"

</VirtualHost>
EOF'
sudo a2enmod rewrite
sudo a2ensite securegrades
sudo a2enmod ssl
composer install
npm install
npm run prod
cp env.prod .env
php artisan key:generate
php artisan migrate
cd ..
sudo mv secure-grades /var/www/
cd /var/www/
sudo chown -R www-data:www-data secure-grades # so even if the server is hacked, hacker can only acts as the www-data user
sudo systemctl restart apache2