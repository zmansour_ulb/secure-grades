<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Secure Grades</title>

    </head>
    <body>
        <div>
            <a href="{{ route('logout') }}" class="text-sm text-gray-700 underline">Log out</a>
        </div>
        <div>
            DASHBOARD
        </div>
    </body>
</html>
