<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Secure Grades</title>

        <style>
        .errors {
            color: #ff1200;
        }
        </style>

    </head>
    <body>
    <h2>SECURE ONLINE CALENDAR</h2>
        <h3>LOGIN</h3>
            <form method="POST" action="/login">
                @csrf
                <input type="hidden" id="timestamp-login" name="timestamp">
                <input type="hidden" id="nonce-login" name="nonce">
                <label for="email">E-mail</label><br>
                <input type="email" id="email" name="email"><br>
                <label for="password">Password</label><br>
                <input type="password" id="password" name="password"><br><br>
                <input id="submit-login" type="submit" value="Submit">
            </form>
            <div class="errors">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        </div>
    </body>
</html>
