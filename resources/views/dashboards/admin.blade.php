<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Secure Grades</title>

        <script src="/js/app.js"></script>

        <style>
        table {
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        td, th {
            border: 1px solid #dddddd;
            text-align: left;
            padding: 8px;
        }

        tr:nth-child(even) {
            background-color: #dddddd;
        }
        .grade {
            color: #0012ff;
        }
        .errors {
            color: #ff1200;
        }
        .margin-bottom-48px {
            margin-bottom: 48px;
        }
        </style>

    </head>
    <body>
        <div>
            <a href="{{ route('logout') }}" class="text-sm text-gray-700 underline">Log out</a><br>
        </div>
        <h2>DASHBOARD</h2>
        <div class="errors">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div><br>
        <h3>Add new user</h3>
        <p>(The password must contain at least 12 characters, one uppercase letter, one lowercase letter, one digit, one special character)</p>
        <form method="POST" action="/users">
            @csrf
            <input type="hidden" id="timestamp-users" name="timestamp">
            <input type="hidden" id="nonce-users" name="nonce">
            <label for="firstname">Firstname</label><br>
            <input type="text" id="firstname" name="firstname"><br>
            <label for="lastname">Lastname</label><br>
            <input type="text" id="lastname" name="lastname"><br>
            <label for="email">E-mail</label><br>
            <input type="email" id="email" name="email"><br>
            <label for="password">Password</label><br>
            <input type="password" id="password" name="password"><br>
            <label for="status">Status</label><br>
            <select name="status" id="status">
                <option value="">--Please choose a status--</option>
                <option value="ADM">Administrator</option>
                <option value="TEA">Teacher</option>
                <option value="STU">Student</option>
            </select><br><br>
            <input id="submit-users" type="submit" value="Submit">
        </form><br>
        <h3>Add new course</h3>
        <form method="POST" action="/courses">
            @csrf
            <input type="hidden" id="timestamp-courses" name="timestamp">
            <input type="hidden" id="nonce-courses" name="nonce">
            <input type="text" id="label" name="label">
            <input id="submit-courses" type="submit" value="Add course">
        </form><br>
        <h3>Administrators</h3>
        <table>
            <tr>
                <th>Firstname</th>
                <th>Lastname</th>
                <th>E-mail</th>
            </tr>
            @foreach ($admins as $user)
            <tr>
                <td>{{ $user->firstname }}</td>
                <td>{{ $user->lastname }}</td>
                <td>{{ $user->email }}</td>
            </tr>
            @endforeach
        </table><br>
        <h3>Teachers</h3>
        <table>
            <tr>
                <th>Firstname</th>
                <th>Lastname</th>
                <th>E-mail</th>
                <th>Teached courses</th>
            </tr>
            @foreach ($teachers as $user)
            <tr>
                <td>{{ $user->firstname }}</td>
                <td>{{ $user->lastname }}</td>
                <td>{{ $user->email }}</td>
                <td>
                    <ul>
                        @foreach ($user->teached_courses as $course)
                        <li>
                            <p>{{ $course->label }}</p>
                        </li>
                        @endforeach
                    </ul>
                    <form method="POST" action="/courses/teachers">
                        @csrf
                        <input type="hidden" id="timestamp-courses-teachers" name="timestamp">
                        <input type="hidden" id="nonce-courses-teachers" name="nonce">
                        <input type="hidden" id="teacherId" name="teacherId" value="{{ $user->id }}">
                        <select name="courseId" id="courseId">
                            <option value="">--Please choose a course--</option>
                            @foreach ($courses as $course)
                            <option value="{{ $course->id }}">{{ $course->label }}</option>
                            @endforeach
                        </select><br><br>
                        <input id="submit-courses-teachers" type="submit" value="Add course">
                    </form>
                </td>
            </tr>
            @endforeach
        </table><br>
        <h3>Courses</h3>
        <table>
            <tr>
                <th>Label</th>
            </tr>
            @foreach ($courses as $course)
            <tr>
                <td>{{ $course->label }}</td>
            </tr>
            @endforeach
        </table><br>
        <h3>Students</h3>
        <table class="margin-bottom-48px">
            <tr>
                <th>Firstname</th>
                <th>Lastname</th>
                <th>E-mail</th>
                <th>Registered courses</th>
            </tr>
            @foreach ($students as $user)
            <tr>
                <td>{{ $user->firstname }}</td>
                <td>{{ $user->lastname }}</td>
                <td>{{ $user->email }}</td>
                <td>
                    <ul>
                        @foreach ($user->registered_courses as $course)
                        <li style="margin-top: auto; margin-bottom: auto; display: flex;">
                            <p style="margin-right: 16px;">{{ $course->label }}:</p>
                            @if (is_numeric($course->grade))
                            <p class="grade">{{ $course->grade }}/20</p>                            
                            @else
                            <form style="margin-top: auto; margin-bottom: auto; display: flex;" method="POST" action="/courses/grades">
                                @csrf
                                <input type="hidden" id="timestamp-courses-grades" name="timestamp">
                                <input type="hidden" id="nonce-courses-grades" name="nonce">
                                <input type="hidden" id="courseId" name="courseId" value="{{ $course->id }}">
                                <input type="hidden" id="studentId" name="studentId" value="{{ $user->id }}">
                                <input type="number" id="grade" name="grade"><br><br>
                                <input id="submit-courses-grades" type="submit" value="Grade">
                            </form>
                            @endif
                        </li>
                        @endforeach
                    </ul>
                    <form method="POST" action="/courses/students">
                        @csrf
                        <input type="hidden" id="timestamp-courses-students" name="timestamp">
                        <input type="hidden" id="nonce-courses-students" name="nonce">
                        <input type="hidden" id="studentId" name="studentId" value="{{ $user->id }}">
                        <select name="courseId" id="courseId">
                            <option value="">--Please choose a course--</option>
                            @foreach ($courses as $course)
                            <option value="{{ $course->id }}">{{ $course->label }}</option>
                            @endforeach
                        </select><br><br>
                        <input id="submit-courses-students" type="submit" value="Add course">
                    </form>
                </td>
            </tr>
            @endforeach
        </table>
    </body>
</html>
