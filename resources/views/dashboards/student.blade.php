<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Secure Grades</title>

        <style>
        table {
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        td, th {
            border: 1px solid #dddddd;
            text-align: left;
            padding: 8px;
        }

        tr:nth-child(even) {
            background-color: #dddddd;
        }
        .grade {
            color: #0012ff;
        }
        .errors {
            color: #ff1200;
        }
        .margin-bottom-48px {
            margin-bottom: 48px;
        }
        </style>

    </head>
    <body>
        <div>
            <a href="{{ route('logout') }}" class="text-sm text-gray-700 underline">Log out</a><br>
        </div>
        <h2>DASHBOARD</h2>
        <p>Firstname: {{ Auth::user()->firstname }}</p>
        <p>Lastname: {{ Auth::user()->lastname }}</p>
        <p>E-mail: {{ Auth::user()->email }}</p>
        <h3>Grades</h3>
        <table class="margin-bottom-48px">
            <tr>
                <th>Label</th>
                <th>Grade</th>
            </tr>
            @foreach ($courses as $course)
            <tr>
                <td>{{ $course->label }}</td>
                @if (is_numeric($course->grade))
                <td>{{ $course->grade }}/20</td>
                @else
                <td>NA</td>
                @endif
            </tr>
            @endforeach
        </table>
    </body>
</html>
