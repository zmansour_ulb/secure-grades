<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Secure Grades</title>

        <script src="/js/app.js"></script>

        <style>
        table {
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        td, th {
            border: 1px solid #dddddd;
            text-align: left;
            padding: 8px;
        }

        tr:nth-child(even) {
            background-color: #dddddd;
        }
        .grade {
            color: #0012ff;
        }
        .errors {
            color: #ff1200;
        }
        .margin-bottom-48px {
            margin-bottom: 48px;
        }
        </style>

    </head>
    <body>
        <div>
            <a href="{{ route('logout') }}" class="text-sm text-gray-700 underline">Log out</a><br>
        </div>
        <h2>DASHBOARD</h2>
        <div class="errors">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div><br>
        <h3>Students</h3>
        <table class="margin-bottom-48px">
            <tr>
                <th>Firstname</th>
                <th>Lastname</th>
                <th>E-mail</th>
                <th>Registered courses</th>
            </tr>
            @foreach ($students as $user)
            <tr>
                <td>{{ $user->firstname }}</td>
                <td>{{ $user->lastname }}</td>
                <td>{{ $user->email }}</td>
                <td>
                    <ul>
                        @foreach ($user->registered_courses as $course)
                        <li style="margin-top: auto; margin-bottom: auto; display: flex;">
                            <p style="margin-right: 16px;">{{ $course->label }}</p>
                            @if (is_numeric($course->grade))
                            <p class="grade">{{ $course->grade }}/20</p>                            
                            @else
                            <form style="margin-top: auto; margin-bottom: auto; display: flex;" method="POST" action="/courses/grades">
                                @csrf
                                <input type="hidden" id="timestamp-courses-grades" name="timestamp">
                                <input type="hidden" id="nonce-courses-grades" name="nonce">
                                <input type="hidden" id="courseId" name="courseId" value="{{ $course->id }}">
                                <input type="hidden" id="studentId" name="studentId" value="{{ $user->id }}">
                                <input type="number" id="grade" name="grade"><br><br>
                                <input id="submit-courses-grades" type="submit" value="Grade">
                            </form>
                            @endif
                        </li>
                        @endforeach
                    </ul>
                </td>
            </tr>
            @endforeach
        </table>
    </body>
</html>
