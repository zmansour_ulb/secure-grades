require('./bootstrap');

const randomString = (length) => {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    for(var i = 0; i < length; i++) {
        text += possible.charAt(Math.floor(Math.random() * possible.length));
    }
    return text;
}
const genTimestamp = () => {
    return Date.parse(new Date(Date.now()))
}

window.onload = () => {
    if (document.querySelectorAll('[id=submit-users]')) {
        document.querySelectorAll('[id=submit-users]').forEach((elem, index) => {
            elem.onclick = () => {
                document.querySelectorAll('[id=timestamp-users]')[index].value = genTimestamp()
                document.querySelectorAll('[id=nonce-users]')[index].value = randomString(128)
            }
        })
    }
    if (document.querySelectorAll('[id=submit-courses]')) {
        document.querySelectorAll('[id=submit-courses]').forEach((elem, index) => {
            elem.onclick = () => {
                document.querySelectorAll('[id=timestamp-courses]')[index].value = genTimestamp()
                document.querySelectorAll('[id=nonce-courses]')[index].value = randomString(128)
            }
        })
    }
    if (document.querySelectorAll('[id=submit-courses-teachers]')) {
        document.querySelectorAll('[id=submit-courses-teachers]').forEach((elem, index) => {
            elem.onclick = () => {
                document.querySelectorAll('[id=timestamp-courses-teachers]')[index].value = genTimestamp()
                document.querySelectorAll('[id=nonce-courses-teachers]')[index].value = randomString(128)
            }
        })
    }
    if (document.querySelectorAll('[id=submit-courses-grades]')) {
        document.querySelectorAll('[id=submit-courses-grades]').forEach((elem, index) => {
            elem.onclick = () => {
                document.querySelectorAll('[id=timestamp-courses-grades]')[index].value = genTimestamp()
                document.querySelectorAll('[id=nonce-courses-grades]')[index].value = randomString(128)
            }
        })
    }
    if (document.querySelectorAll('[id=submit-courses-students]')) {
        document.querySelectorAll('[id=submit-courses-students]').forEach((elem, index) => {
            elem.onclick = () => {
                document.querySelectorAll('[id=timestamp-courses-students]')[index].value = genTimestamp()
                document.querySelectorAll('[id=nonce-courses-students]')[index].value = randomString(128)
            }
        })
    }
    if (document.querySelectorAll('[id=submit-login')) {
        document.querySelectorAll('[id=submit-login').forEach((elem, index) => {
            elem.onclick = () => {
                document.querySelectorAll('[id=timestamp-login')[index].value = genTimestamp()
                document.querySelectorAll('[id=nonce-login')[index].value = randomString(128)
            }
        })
    }
}