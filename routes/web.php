<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\LoginController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\CourseController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('login');
})->name('home');
Route::get('/login', [LoginController::class, 'index'])->name('login');
Route::get('/logout', [LoginController::class, 'logout'])->name('logout')->middleware(['auth']);
Route::get('/dashboard', [DashboardController::class, 'index'])->name('dashboard')->middleware(['auth']);

Route::post('/login', [LoginController::class, 'authenticate'])->middleware('throttle:5,1',);
Route::post('/users', [UserController::class, 'create'])->middleware(['auth', 'isadmin']);
Route::post('/courses', [CourseController::class, 'create'])->middleware(['auth', 'isadmin']);
Route::post('/courses/grades', [CourseController::class, 'grade'])->middleware(['auth', 'nostudent']);
Route::post('/courses/teachers', [CourseController::class, 'addTeacher'])->middleware(['auth', 'isadmin']);
Route::post('/courses/students', [CourseController::class, 'addStudent'])->middleware(['auth', 'isadmin']);