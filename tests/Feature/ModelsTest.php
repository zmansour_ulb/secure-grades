<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Hash;

use Tests\TestCase;

use App\Models\User;
use App\Models\Grade;
use App\Models\Course;
use App\Models\CourseTeacher;
use App\Models\CourseStudent;


class ModelsTest extends TestCase
{
    private $teachers = [];
    private $students = [];
    private $courses = [];
    private $grades = [];

    private $nbAdminsAtStart = 0;
    private $nbTeachersAtStart = 0;
    private $nbStudentsAtStart = 0;
    private $nAdmins = 0;
    private $nbTeachers = 2;
    private $nbStudents = 10;
    private $nbCourses = 3;

    public function setUp() : void
    {
        parent::setUp();

        $this->nbAdminsAtStart = User::where('status', 'ADM')->count();
        $this->nbTeachersAtStart = User::where('status', 'TEA')->count();
        $this->nbStudentsAtStart = User::where('status', 'STU')->count();
        
        for ($i = 0; $i < $this->nbTeachers; $i++) {
            array_push($this->teachers, User::create([
                'status' => 'TEA',
                'firstname' => 'Teacher_' . $i,
                'lastname' => 'Mc Teacher_' . $i,
                'email' => 'teacher' . $i . '@tea.tea',
                'password' => Hash::make('teacher_' . $i),
            ]));
        }

        for ($i = 0; $i < $this->nbStudents; $i++) {
            array_push($this->students, User::create([
                'status' => 'STU',
                'firstname' => 'Student_' . $i,
                'lastname' => 'Mc Student_' . $i,
                'email' => 'student' . $i . '@stu.stu',
                'password' => Hash::make('student_' . $i),
            ]));
        }

        for ($i = 0; $i < $this->nbCourses; $i++) {
            array_push($this->courses, Course::create([
                'label' => 'Course_' . $i,
            ]));
        }

        // Course_0 Teachers
        CourseTeacher::create([
            'teacherId' => $this->teachers[0]->id,
            'courseId'=> $this->courses[0]->id,
        ]);
        // Course_1 Teachers
        CourseTeacher::create([
            'teacherId' => $this->teachers[1]->id,
            'courseId'=> $this->courses[1]->id,
        ]);
        // Course_2 Teachers
        CourseTeacher::create([
            'teacherId' => $this->teachers[0]->id,
            'courseId'=> $this->courses[2]->id,
        ]);
        CourseTeacher::create([
            'teacherId' => $this->teachers[1]->id,
            'courseId'=> $this->courses[2]->id,
        ]);
        // Course_0 Students
        CourseStudent::create([
            'studentId' => $this->students[0]->id,
            'courseId' => $this->courses[0]->id,
        ]);
        CourseStudent::create([
            'studentId' => $this->students[1]->id,
            'courseId' => $this->courses[0]->id,
        ]);
        CourseStudent::create([
            'studentId' => $this->students[3]->id,
            'courseId' => $this->courses[0]->id,
        ]);
        CourseStudent::create([
            'studentId' => $this->students[5]->id,
            'courseId' => $this->courses[0]->id,
        ]);
        CourseStudent::create([
            'studentId' => $this->students[7]->id,
            'courseId' => $this->courses[0]->id,
        ]);
        CourseStudent::create([
            'studentId' => $this->students[8]->id,
            'courseId' => $this->courses[0]->id,
        ]);
        CourseStudent::create([
            'studentId' => $this->students[9]->id,
            'courseId' => $this->courses[0]->id,
        ]);
        // Course_1 Students
        CourseStudent::create([
            'studentId' => $this->students[1]->id,
            'courseId' => $this->courses[1]->id,
        ]);
        CourseStudent::create([
            'studentId' => $this->students[2]->id,
            'courseId' => $this->courses[1]->id,
        ]);
        CourseStudent::create([
            'studentId' => $this->students[3]->id,
            'courseId' => $this->courses[1]->id,
        ]);
        CourseStudent::create([
            'studentId' => $this->students[8]->id,
            'courseId' => $this->courses[1]->id,
        ]);
        CourseStudent::create([
            'studentId' => $this->students[9]->id,
            'courseId' => $this->courses[1]->id,
        ]);
        // Course_2 Students
        CourseStudent::create([
            'studentId' => $this->students[2]->id,
            'courseId' => $this->courses[2]->id,
        ]);
        CourseStudent::create([
            'studentId' => $this->students[4]->id,
            'courseId' => $this->courses[2]->id,
        ]);
        CourseStudent::create([
            'studentId' => $this->students[8]->id,
            'courseId' => $this->courses[2]->id,
        ]);

    }

    public function testCount()
    {
        $this->assertEquals(
            User::count() - ($this->nbAdminsAtStart + $this->nbTeachersAtStart + $this->nbStudentsAtStart),
            $this->nbTeachers + $this->nbStudents
        );
        $this->assertEquals(User::where('status', 'TEA')->count() - $this->nbTeachersAtStart, $this->nbTeachers);
        $this->assertEquals(User::where('status', 'STU')->count() - $this->nbStudentsAtStart, $this->nbStudents);
        $this->assertEquals(Course::count(), $this->nbCourses);

        error_log('TEACHERS:');
        error_log('');
        foreach ($this->teachers as $teacher) {
            error_log(json_encode(User::with('teached_courses')->find($teacher->id)));
            error_log('');
        }
        error_log('STUDENTS:');
        error_log('');
        foreach ($this->students as $student) {
            error_log(json_encode(User::with('registered_courses')->find($student->id)));
            error_log('');
        }
        error_log('COURSES:');
        error_log('');
        foreach ($this->courses as $course) {
            error_log(json_encode(Course::with('teachers', 'students')->find($course->id)));
            error_log('');
        }

        error_log('GRADES:');
        error_log('');
        $grade = Grade::create([
            'value' => 10,
            'studentId' => $this->students[0]->id,
            'courseId' => $this->courses[0]->id,
        ]);
        error_log(json_encode($grade));
    }

    public function tearDown() : void
    {
        // Destroy all models
        foreach ($this->teachers as $teacher) {
            User::destroy($teacher->id);
        }
        foreach ($this->students as $student) {
            User::destroy($student->id);
        }
        foreach ($this->courses as $course) {
            Course::destroy($course->id);
        }

        parent::tearDown();
    }



}