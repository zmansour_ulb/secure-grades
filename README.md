# Installation

```bash
git clone https://gitlab.com/zmansour_ulb/secure-grades.git
cd secure-grades/
./make.sh
```

The [make.sh](make.sh) script will need sudo rights for certain operations. You might be asked multiple times to enter your password in order to complete the installation (up to 3 times when I ran it). Note that the installation took ~20 minutes on my side.

# Default data

## User credentials

### Administrator:

- Zeus McAdmin
    - mail: zeus.admin@uni.be
    - pass: zg1T1J97Sx.k7BYjVcFaj

### Teachers

- Appolo McTeacher
    - mail: appolo.teacher@uni.be
    - pass: kwz2AKBrQ0.rz7mRwiUBd
- Artemis McTeacher
    - mail: artemis.teacher@uni.be
    - pass: nVPtexWFqc.7e02XzfMyo

### Students
- Ares McStudent
    - mail: ares.student@uni.be
    - pass: UUcU5Yeggo.IT3ESKlC3u
- Demeter McStudent
    - mail: demeter.student@uni.be
    - pass: O02eg447Hf.8cN3Re3Uo1
- Dionysus McStudent
    - mail: dionysus.student@uni.be
    - pass: QXiSdn4Q9M.0PtiIO9Gb2
- Hades McStudent
    - mail: hades.student@uni.be
    - pass: FMgWGo2BZL.UXUSne1mcY
- Hephaestus McStudent
    - mail: hephaestus.student@uni.be
    - pass: jq9zVsUdpk.eRHUOrdxRj
- Hera McStudent
    - mail: hera.student@uni.be
    - pass: GkDTiVF8sR.Zsdca8PQDz
- Hermes McStudent
    - mail: hermes.student@uni.be
    - pass: TVZgbCFWtG.By1B5UkRy2
- Hestia McStudent
    - mail: hestia.student@uni.be
    - pass: EOwdYU9fq7.BJcOnZd1LE
- Poseidon McStudent
    - mail: poseidon.student@uni.be
    - pass: 1YyRH8Mv6E.CTkvhaU2FQ
- Aphrodite McStudent
    - mail: aphrodite.student@uni.be
    - pass: nQANUN5bc6.Ce3kYO0lTS

## Courses

- MATH-H100 Analysis I
- MECA-H200 Mechanics II
- ELEC-H310 Digital Electronics

The admininistrator dashboard gives the following information:
- for each teacher: which courses he/she gives
- for each student: which courses he/she is registered for

<br>
More information in the [report](report.pdf).