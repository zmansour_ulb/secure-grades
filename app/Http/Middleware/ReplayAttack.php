<?php
/**
 * This file is part of ruogoo.
 *
 * Created by HyanCat.
 *
 * Copyright (C) HyanCat. All rights reserved.
 */

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Cache\Repository as Cache;
use Illuminate\Contracts\Config\Repository as Config;

class ReplayAttack
{
    /**
     * @var \Illuminate\Contracts\Cache\Repository
     */
    private $cache;

    /**
     * @var bool
     */
    private $enabled;

    /**
     * Expire time interval in seconds.
     * @var int
     */
    private $expire;

    public function __construct(Config $config, Cache $cache)
    {
        $this->enabled = $config->get('replay_attack.enabled');
        $this->expire  = (int)$config->get('replay_attack.expire');
        $this->cache   = $cache;
    }

    /**
     * @var \Illuminate\Http\Request
     */
    private $request;

    public function handle($request, Closure $next)
    {
        if ( $request->route()->getName() == 'home' || $request->is('logout') || $request->is('dashboard') || $request->is('login')) {
            return $next($request);
        }
        $this->request = $request;
        if (!$this->enabled) {
            return $next($request);
        }

        if (!$this->checkTimestamp()) {
            return response()->json([
                'message' => 'This looks like a replay attack...',
            ], 403);
        }
        if (!$this->checkNonce()) {
            return response()->json([
                'message' => 'This looks like a replay attack...',
            ], 403);
        }

        return $next($request);
    }

    protected function checkTimestamp(): bool
    {
        if (!$this->request->has('timestamp')) {
            return false;
        }
        $timestamp = $this->request->timestamp;
        return $this->isExpired($timestamp);
    }

    protected function checkNonce(): bool
    {
        if (!$this->request->has('nonce')) {
            return false;
        }
        $nonce = $this->request->nonce;
        $key   = 'api:nonce:' . $nonce;
        if ($this->cache->has($key)) {
            return false;
        }
        $this->cache->put($key, time(), $this->expire / 60);

        return true;
    }

    protected function isExpired($timestamp): bool
    {
        return abs(1000 * time() - (int)$timestamp) <= 1000 * $this->expire;
    }
}