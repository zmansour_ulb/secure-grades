<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class NoStudent
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if ($request->user()->status == 'STU') {
            return abort(403);
        }
        return $next($request);
    }
}
