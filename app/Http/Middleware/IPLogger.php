<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;


class IPLogger
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $data = $request->toArray();
        unset($data['password']); // no passwords in the logs
        unset($data['_token']); // no passwords in the logs
        Log::info(
            (Auth::user() ? '[userID #' . Auth::id() . '] ' : '[Guest] ')
            . $request->method() . ' ' . $request->path() . ' from IP ' . $request->ip(), $data
        );
        return $next($request);
    }
}
