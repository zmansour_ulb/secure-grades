<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;

use App\Models\User;

class UserController extends Controller
{
    public function create(Request $request)
    {
        $request->validate([
            'firstname' => [
                'required',
                'string',
                'max:256',
                "regex:/^[a-zA-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð ,.\'-]+$/",
            ],
            'lastname' => [
                'required',
                'string',
                'max:256',
                "regex:/^[a-zA-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð ,.\'-]+$/",
            ],
            'email' => [
                'required',
                'string',
                'email',
                'max:256',
            ], 
            'password' => [
                'required',
                'string',
                'min:12',
                'max:128',
                'regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[\.!@#\$%\^&\*])(?=.{12,})/',
            ],
            'status' => [
                'required',
                'string',
                'regex:/^(ADM|TEA|STU)$/'
            ],
        ]);

        DB::beginTransaction();
        try {
            User::create([
                'status' => $request->status,
                'firstname' => $request->firstname,
                'lastname' => $request->lastname,
                'email' => $request->email,
                'password' => Hash::make($request->password),
            ]);
        } catch(\Exception $e) {
            DB::rollback();
            Log::error('Error when creating a new user.', [
                'auth_user' => Auth::user(),
                'user' => [
                    'status' => $request->status,
                    'firstname' => $request->firstname,
                    'lastname' => $request->lastname,
                    'email' => $request->email,
                ],
                'error' => $e,
            ]);
            throw $e;
        }
        DB::commit();

        return redirect()->back()->with('success', 'Thank you!');
    }
}
