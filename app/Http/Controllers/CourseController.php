<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

use App\Models\User;
use App\Models\Course;
use App\Models\CourseStudent;
use App\Models\CourseTeacher;

class CourseController extends Controller
{
    public function create(Request $request)
    {
        $request->validate([
            'label' => [
                'required',
                'string',
                'max:256',
                "regex:/^[a-zA-Z0-9àáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð ,.\'-]+$/",
            ],
        ]);

        DB::beginTransaction();
        try {
            Course::create([
                'label' => $request->label,
            ]);
        } catch(\Exception $e) {
            DB::rollback();
            Log::error('Error when creating a new course.', [
                'auth_user' => Auth::user(),
                'course' => [
                    'label' => $request->label,
                ],
                'error' => $e,
            ]);
            throw $e;
        }
        DB::commit();

        return redirect()->back()->with('success', 'Thank you!');
    }

    public function grade(Request $request)
    {
        $request->validate([
            'grade' => [
                'required',
                'numeric',
                'integer',
                'min:0',
                'max:20',
            ],
            'courseId' => [
                'required',
                'integer',
                'exists:courses,id',
            ],
            'studentId' => [
                'required',
                'integer',
                'exists:users,id',
            ],
        ]);

        if (Auth::user()->status == 'TEA' && !CourseTeacher::where('courseId', $request->courseId)->where('teacherId', Auth::id())->exists()) {
            Log::info('Not a course you teach, you cannot grade it.', [
                'auth_user' => Auth::user(),
                'student' => [
                    'id' => $request->studentId,
                ],
                'course' => [
                    'id' => $request->courseId,
                    'grade' => $request->grade,
                ],
            ]);
            return back()->withErrors([
                'message' => 'Not a course you teach, you cannot grade it.'
            ]);
        }

        $csq = CourseStudent::where('courseId', $request->courseId)->where('studentId', $request->studentId);
        if (!$csq->exists()) {
            Log::info('No student is registered for that course.', [
                'auth_user' => Auth::user(),
                'student' => [
                    'id' => $request->studentId,
                ],
                'course' => [
                    'id' => $request->courseId,
                    'grade' => $request->grade,
                ],
            ]);
            return back()->withErrors([
                'message' => 'No student is registered for that course.'
            ]);
        } else {
            if ($csq->first()->grade != null) {
                Log::info('Grade already exists.', [
                    'auth_user' => Auth::user(),
                    'student' => [
                        'id' => $request->studentId,
                    ],
                    'course' => [
                        'id' => $request->courseId,
                        'grade' => $request->grade,
                    ],
                ]);
                return back()->withErrors([
                    'message' => 'Grade already exists.'
                ]);
            }
        }

        DB::beginTransaction();
        try {
            $cs = $csq->first();
            $cs->grade = $request->grade;
            $cs->save();
        } catch(\Exception $e) {
            DB::rollback();
            Log::error('Error when grading a student for a course.', [
                'auth_user' => Auth::user(),
                'student' => [
                    'id' => $request->studentId,
                ],
                'course' => [
                    'id' => $request->courseId,
                    'grade' => $request->grade,
                ],
                'error' => $e,
            ]);
            throw $e;
        }
        DB::commit();

        return redirect()->back()->with('success', 'Thank you!');
    }

    public function addTeacher(Request $request)
    {
        $request->validate([
            'courseId' => [
                'required',
                'integer',
                'exists:courses,id',
            ],
            'teacherId' => [
                'required',
                'integer',
                'exists:users,id',
            ],
        ]);

        $csq = CourseTeacher::where('courseId', $request->courseId)->where('teacherId', $request->teacherId);
        if ($csq->exists()) {
            Log::error('Teacher already teaching that course.', [
                'auth_user' => Auth::user(),
                'teacher' => [
                    'id' => $request->teacherId,
                ],
                'course' => [
                    'id' => $request->courseId,
                ],
            ]);
            return back()->withErrors([
                'message' => 'Teacher already teaching that course.',
            ]);
        }
        if (User::find($request->teacherId)->status != 'TEA') {
            Log::error('No teacher found for that ID.', [
                'auth_user' => Auth::user(),
                'teacher' => [
                    'id' => $request->teacherId,
                ],
                'course' => [
                    'id' => $request->courseId,
                ],
            ]);
            return back()->withErrors([
                'message' => 'No teacher found for that ID.'
            ]);
        }

        DB::beginTransaction();
        try {
            CourseTeacher::create([
                'courseId' => $request->courseId,
                'teacherId' => $request->teacherId,
            ]);
        } catch(\Exception $e) {
            DB::rollback();
            Log::error('Error when associating a teacher to a course.', [
                'auth_user' => Auth::user(),
                'teacher' => [
                    'id' => $request->teacherId,
                ],
                'course' => [
                    'id' => $request->courseId,
                ],
                'error' => $e,
            ]);
            throw $e;
        }
        DB::commit();

        return redirect()->back()->with('success', 'Thank you!');
    }

    public function addStudent(Request $request)
    {
        $request->validate([
            'courseId' => [
                'required',
                'integer',
                'exists:courses,id',
            ],
            'studentId' => [
                'required',
                'integer',
                'exists:users,id',
            ],
        ]);

        $csq = CourseStudent::where('courseId', $request->courseId)->where('studentId', $request->studentId);
        if ($csq->exists()) {
            Log::error('Student already registered for that course.', [
                'auth_user' => Auth::user(),
                'student' => [
                    'id' => $request->studentId,
                ],
                'course' => [
                    'id' => $request->courseId,
                ],
            ]);
            return back()->withErrors([
                'message' => 'Student already registered for that course.'
            ]);
        }
        if (User::find($request->studentId)->status != 'STU') {
            Log::error('No student found for that ID.', [
                'auth_user' => Auth::user(),
                'student' => [
                    'id' => $request->studentId,
                ],
                'course' => [
                    'id' => $request->courseId,
                ],
            ]);
            return back()->withErrors([
                'message' => 'No student found for that ID.'
            ]);
        }

        DB::beginTransaction();
        try {
            CourseStudent::create([
                'courseId' => $request->courseId,
                'studentId' => $request->studentId,
            ]);
        } catch(\Exception $e) {
            DB::rollback();
            Log::error('Error when associating a student to a course.', [
                'auth_user' => Auth::user(),
                'student' => [
                    'id' => $request->studentId,
                ],
                'course' => [
                    'id' => $request->courseId,
                ],
                'error' => $e,
            ]);
            throw $e;
        }
        DB::commit();

        return redirect()->back()->with('success', 'Thank you!');
    }
}
