<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

use App\Models\User;
use App\Models\Course;
use App\Models\CourseStudent;
use App\Models\CourseTeacher;

class DashboardController extends Controller
{
    public function index()
    {
        if (Auth::user()->status == 'ADM') {
            // Admin View
            $admins = User::where('status', 'ADM')->get();
            $teachers = User::with('teached_courses')->where('status', 'TEA')->get();
            $courses = Course::all();
            $students = User::with('registered_courses')->where('status', 'STU')->get();
            foreach ($students as $student) {
                foreach ($student->registered_courses as $course) {
                    $grade = CourseStudent::where('courseId', $course->id)->where('studentId', $student->id)->first()->grade;
                    $course['grade'] = $grade;
                }
            }
            return view('dashboards.admin', [
                'admins' => $admins,
                'teachers' => $teachers,
                'courses' => $courses,
                'students' => $students,
            ]);
        } else if (Auth::user()->status == 'TEA') {
            // Teacher View
            $my_students = [];
            $students = User::with('registered_courses')->where('status', 'STU')->get();
            foreach ($students as $student) {
                $my_courses = [];
                foreach ($student->registered_courses as $course) {
                    if (CourseTeacher::where('courseId', $course->id)->where('teacherId', Auth::id())->exists()) {
                        $grade = CourseStudent::where('courseId', $course->id)->where('studentId', $student->id)->first()->grade;
                        $course['grade'] = $grade;
                        array_push($my_courses, $course);
                    }
                }
                if (count($my_courses) > 0) {
                    $student->registered_courses = $my_courses;
                    array_push($my_students, $student);
                }
            }
            return view('dashboards.teacher', [
                'students' => $my_students,
            ]);
        } else if (Auth::user()->status == 'STU') {
            $student = User::with('registered_courses')->find(Auth::id());
            foreach ($student->registered_courses as $course) {
                if (CourseStudent::where('courseId', $course->id)->where('studentId', Auth::id())->exists()) {
                    $grade = CourseStudent::where('courseId', $course->id)->where('studentId', $student->id)->first()->grade;
                    $course['grade'] = $grade;
                }
            }
            return view('dashboards.student', [
                'courses' => $student->registered_courses,
            ]);
        }
        Log::warning('A user with an unknown status accessed the dashboard.', [
            'auth_user' => Auth::user(),
        ]);
        return view('dashboard');
    }
}