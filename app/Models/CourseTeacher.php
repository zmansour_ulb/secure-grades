<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\Pivot;

use App\Models\User;
use App\Models\Course;

class CourseTeacher extends Pivot
{
    use HasFactory;

    /**
     * The name of the table used in the SQL request
     * 
     * NECESSARY for Pivot
     * 
     * @var string
     */
    protected $table = 'course_teachers';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'teacherId',
        'courseId',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [

    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [

    ];

    public function course()
    {
        return $this->belongsTo(Course::class, 'courseId', 'id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'teacherId', 'id');
    }
}
