<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\Pivot;

use App\Models\User;
use App\Models\Course;

class CourseStudent extends Pivot
{
    use HasFactory;

    /**
     * The name of the table used in the SQL request
     * 
     * NECESSARY for Pivot
     * 
     * @var string
     */
    protected $table = 'course_students';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'studentId',
        'courseId',
        'grade',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [

    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [

    ];

    public function course()
    {
        return $this->belongsTo(Course::class, 'courseId', 'id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'studentId', 'id');
    }
}
