<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

use App\Models\Course;
use App\Models\CourseTeacher;
use App\Models\CourseStudent;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'status',
        'firstname',
        'lastname',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function teached_courses()
    {
        return $this->belongsToMany(Course::class, 'course_teachers', 'teacherId', 'courseId')
                    ->using(CourseTeacher::class);
    }

    public function registered_courses()
    {
        return $this->belongsToMany(Course::class, 'course_students', 'studentId', 'courseId')
                    ->using(CourseStudent::class);
    }
}
