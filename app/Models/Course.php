<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Models\User;
use App\Models\CourseTeacher;
use App\Models\CourseStudent;

class Course extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'label',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [

    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [

    ];

    public function teachers()
    {
        return $this->belongsToMany(User::class, 'course_teachers', 'courseId', 'teacherId',)
                    ->using(CourseTeacher::class);
    }

    public function students()
    {
        return $this->belongsToMany(User::class, 'course_students', 'courseId', 'studentId',)
                    ->using(CourseStudent::class);
    }
}
