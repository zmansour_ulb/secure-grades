<?php

namespace App\Observers;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;

use App\Models\CourseTeacher;

class CourseTeacherObserver
{
    /**
     * Handle the CourseTeacher "created" event.
     *
     * @param  \App\Models\CourseTeacher  $courseTeacher
     * @return void
     */
    public function created(CourseTeacher $courseTeacher)
    {
        Log::info('Teacher associated to a course.', [
            'auth_user' => Auth::user(),
            'courseTeacher' => $courseTeacher,
        ]);
    }

    /**
     * Handle the CourseTeacher "updated" event.
     *
     * @param  \App\Models\CourseTeacher  $courseTeacher
     * @return void
     */
    public function updated(CourseTeacher $courseTeacher)
    {
        // Never updated.
    }

    /**
     * Handle the CourseTeacher "deleted" event.
     *
     * @param  \App\Models\CourseTeacher  $courseTeacher
     * @return void
     */
    public function deleted(CourseTeacher $courseTeacher)
    {
        // Never deleted.
    }

    /**
     * Handle the CourseTeacher "restored" event.
     *
     * @param  \App\Models\CourseTeacher  $courseTeacher
     * @return void
     */
    public function restored(CourseTeacher $courseTeacher)
    {
        // Never restored.
    }

    /**
     * Handle the CourseTeacher "force deleted" event.
     *
     * @param  \App\Models\CourseTeacher  $courseTeacher
     * @return void
     */
    public function forceDeleted(CourseTeacher $courseTeacher)
    {
        // Never forceDeleted.
    }
}
