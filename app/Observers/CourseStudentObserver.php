<?php

namespace App\Observers;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;

use App\Models\CourseStudent;

class CourseStudentObserver
{
    /**
     * Handle the CourseStudent "created" event.
     *
     * @param  \App\Models\CourseStudent  $courseStudent
     * @return void
     */
    public function created(CourseStudent $courseStudent)
    {
        Log::info('Student associated to a course.', [
            'auth_user' => Auth::user(),
            'courseStudent' => $courseStudent,
        ]);
    }

    /**
     * Handle the CourseStudent "updated" event.
     *
     * @param  \App\Models\CourseStudent  $courseStudent
     * @return void
     */
    public function updated(CourseStudent $courseStudent)
    {
        Log::info('Student graded for a course.', [
            'auth_user' => Auth::user(),
            'courseStudent' => $courseStudent,
        ]);
    }

    /**
     * Handle the CourseStudent "deleted" event.
     *
     * @param  \App\Models\CourseStudent  $courseStudent
     * @return void
     */
    public function deleted(CourseStudent $courseStudent)
    {
        // Never deleted.
    }

    /**
     * Handle the CourseStudent "restored" event.
     *
     * @param  \App\Models\CourseStudent  $courseStudent
     * @return void
     */
    public function restored(CourseStudent $courseStudent)
    {
        // Never restored.
    }

    /**
     * Handle the CourseStudent "force deleted" event.
     *
     * @param  \App\Models\CourseStudent  $courseStudent
     * @return void
     */
    public function forceDeleted(CourseStudent $courseStudent)
    {
        // Never forceDeleted.
    }
}
